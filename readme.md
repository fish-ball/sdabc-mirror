## 安装
将项目 clone 到服务器之后，解压微擎离线安装包 `we7_offline.zip`，安装的时候选择离线安装即可。

>   安装完成之后，将 `develop/app/common/template.func.php` 和 `develop/web/common/template.func.php` 文件
分别覆盖原来的 `app/common/template.func.php` 和 `web/common/template.func.php`。
其实，主要是修改了这两个文件的 `template()` 函数。

