$(function($) {

    var isPage = function(cls) {
        return $('div').hasClass(cls);
    };

    // web/employee/list.html
    if (isPage('web-employee-list')) {
        // Delete employee
        $('td.action a.delete').click(function() {
            var id = $(this).data('id');
            var name = $(this).data('name');
            var url = $(this).data('url');
            if (confirm('确定要删除员工 ' + name + ' 吗？')) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        action: 'delete',
                        id: id
                    },
                    success: function(data) {
                        if (data == 'success') {
                            $('tr.employee-' + id).hide(500);
                        }
                    }
                });
            }
        });
    }

    // web/employee/edit.html
    if (isPage('web-employee-edit')) {
        // employee qrcode
        require(['jquery.qrcode'], function($) {
            var $employee_qrcode = $('.employee-qrcode');
            $employee_qrcode.qrcode({
                render: 'canvas',
                width: 150,
                height: 150,
                text: $employee_qrcode.data('url')
            });
        });
    }

    // web/employee/add.html
    if (isPage('web-employee-add')) {
        // Add employee
        $('td.action a.add').click(function() {
            var uid = $(this).data('uid');
            var nickname = $(this).data('nickname');
            var url = $(this).data('url');
            if (confirm('确定要添加 ' + nickname + ' 为员工吗？')) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        action: 'add',
                        uid: uid
                    },
                    success: function(data) {
                        if (data == 'success') {
                            alert('添加成功！');
                            $('tr.member-' + uid).hide(500);
                        } else {
                            alert('添加失败！');
                        }
                    }
                });
            }
        });
    }

    // web/employee/id.html
    if (isPage('web-employee-id')) {
        // 【添加工号】: 【手动输入】/【excel导入】
        var $select_btn = $('select[name=input_type]');
        $select_btn.change(function() {
            $('input[name=manual]').toggle();
            $('input[name=excel]').toggle();
        });
        // 【工号列表】: 【全选】
        $('input[name=select_all]').change(function() {
            if ($(this).prop('checked')) {
                $('form.list-form input[type=checkbox]').prop('checked', true);
            } else {
                $('form.list-form input[type=checkbox]').prop('checked', false);
            }
        });
        // 【工号列表】:【删除所选】
        $('.action-btn .deleted').click(function() {
            // Firstly, get all selected employee id
            var selected_id = [];
            $('input[name=employee_id]:checked').each(function() {
                selected_id.push($(this).val());
            });
            // Delete employee id
            if (selected_id.length) {
                $.ajax({
                    type: 'POST',
                    url: $(this).data('url'),
                    data: {
                        employee_id: selected_id
                    },
                    success: function(data, status) {
                        if (data == 'success') {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    }

    // web/department/list.html
    if (isPage('web-department-list')) {
        // 【编辑】部门
        $('td.action a.edit').click(function() {
            var numbers = $(this).data('numbers');
            // Hide the show department and then show the input button
            $(this).closest('tr').find('td span').hide()
                .siblings("input[type=text]").show();
            // 隐藏【编辑】和【删除】，显示【取消】和【保存】
            $(this).hide().siblings('.delete').hide()
                .siblings('.cancel').show().siblings('.save').show();
        });
        // 【取消】编辑
        $('td.action a.cancel').click(function() {
            // Hide the input button and then show department name
            $(this).closest('tr').find('td input[type=text]').hide()
                .siblings("span").show();
            // 隐藏【取消】和【保存】，显示【编辑】和【删除】
            $(this).hide().siblings('.save').hide()
                .siblings('.edit').show().siblings('.delete').show();
        });
        // 【保存】
        $('td.action a.save').click(function() {
            $.ajax({
                type: 'POST',
                url: $(this).data('url'),
                data: {
                    action: 'save',
                    department_id: $(this).data('id'),
                    department_name: $(this).closest('tr').find('td input').val()
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        // 【删除】部门
        $('td.action a.delete').click(function() {
            var superior_id = $(this).data('superior_id');
            var department_name = $(this).data('name');
            var confirm_text = superior_id ?
                '确定要删除 ' + department_name + ' 吗？':
                '确定要删除 ' + department_name + ' 吗？注意：该部门下面的二级部门也会删除！';
            if (confirm(confirm_text)) {
                $.ajax({
                    type: 'POST',
                    url: $(this).data('url'),
                    data: {
                        action: 'delete',
                        department_id: $(this).data('id')
                    },
                    success: function(data) {
                        if (data == 'success') {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    }

    // web/statistics/index.html
    if (isPage('web-statistics-index')) {
        // 选择部门
        $('select.superior').change(function() {
            var select_value = $(this).val();
            // 二级部门
            var $subordinate = $('select.subordinate');
            // 将二级部门的选择值设置为空
            $subordinate.val(0);
            // 隐藏二级部门
            $subordinate.hide();
            if ($subordinate.find('option').hasClass('superior-' + select_value)) {
                $subordinate.show();
                // 先隐藏所有二级部门
                $('select.subordinate option').hide();
                // 显示一级部门对应的二级部门
                $('select.subordinate option.superior-' + $(this).val()).show();
            }
        });
    }

});