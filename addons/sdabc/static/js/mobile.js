jQuery(function($) {

    var isPage = function(cls) {
        return $('div').hasClass(cls);
    };

    // mobile/employee/register.html
    if (isPage('mobile-employee-register')) {
        // Check employee id
        var input_tips = function() {
            if ($('input.employee-id').val() != $('input.employee-id2').val()) {
                $('.work-id-again .tips').show();
                $('input[type=submit]').attr('disabled', true);
            } else {
                $('.work-id-again .tips').hide();
                $('input[type=submit]').attr('disabled', false);
            }
        };
        $('.register-form input.employee-id2').change(function() {
            input_tips();
        });
        $('.register-form input.employee-id').change(function() {
            if ($('input.employee-id2').val()) {
                input_tips();
            }
        });
        // Close the error tips
        $('.register-result-tips').click(function() {
            $(this).hide();
        });
        $('.register-result-btn').click(function(e) {
            e.stopPropagation();
        });

        // Verify the employee id and name
        $('.verify-employee').click(function() {
            var employee_id = $('input.employee-id').val();
            var employee_name = $('input.employee-name').val();
            var post_url = $(this).data('url');
            if (employee_id && employee_name && post_url) {
                $.ajax({
                    type: 'POST',
                    url: post_url,
                    data: {
                        action: 'verify',
                        employee_id: employee_id,
                        employee_name: employee_name
                    },
                    success: function(data) {
                        var result = JSON.parse(data);
                        if (result['status'] == 'error') {
                            $('.verify-tip').text(result['content']);
                        }
                        if (result['status'] == 'success') {
                            $('.verify-tip').hide();
                            $('.verify-employee').hide();
                            $('label.superior-department').css('display', 'block');
                            $('input[type=submit]').css('display', 'block');
                        }
                    }
                });
            }
        });
        // Chose superior department
        // First set the value of select element to 0.
        var $department = $('label select');
        $department.val(0);
        // When the select change, update the show layer text
        $department.change(function() {
            // change the show layer text
            $(this).siblings('.show-layer').text($(this).find('option:selected').text())
                    .css('color', '#000000');
        });

        // When the superior department change,
        // and then show the opposite subordinate department
        $('select.superior').change(function() {
            // If the subordinate department is exist
            // show the opposite subordinate department
            var $subordinate = $('.subordinate-department');
            $subordinate.hide();
            $subordinate.val(0);
            $('.show-layer.subordinate').text('请选择二级部门').css('color', '#cccccc');
            var $subordinate_option = $subordinate.find('option');
            $subordinate_option.hide();
            if ($subordinate.find('option').hasClass('superior-' + $(this).val())) {
                $subordinate.css('display', 'block');
                $subordinate.find('option.superior-' + $(this).val()).show();
            }
        });

    }

    // mobile/employee/qrcode.html
    if (isPage('mobile-employee-qrcode')) {
        // Generate employee qrcode
        // var $qrcode_btn = $('.employee-qrcode');
        // $qrcode_btn.qrcode({
        //    render: 'canvas',
        //    text: $qrcode_btn.data('url'),
        // });
    }

    // mobile/employee/message.html
    if (isPage('mobile-employee-message')) {
    }

});
