<?php
/**
 * ims_ec_sdabc_employee_article
 */

defined('IN_IA') or exit('Access Denied!');

$db['ec_sdabc_employee_article'] = array(
    'columns' => array(
        'id' => array(
            'type' => 'int(11)',
            'required' => true,
            'auto_increment' => true,
            'primary' => true,
        ),
        'uniacid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'employee_uid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'article_id' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'created_time' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
    ),
    'comment' => '员工阅读文章记录',
);
