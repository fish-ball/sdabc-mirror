<?php
/**
 * ims_ec_sdabc_departments
 */

defined('IN_IA') or exit('Access Denied!');

$db['ec_sdabc_departments'] = array(
    'columns' => array(
        'id' => array(
            'type' => 'int(11)',
            'required' => true,
            'auto_increment' => true,
            'primary' => true,
        ),
        'uniacid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'name' => array(
            'type' => 'varchar(30)',
            'required' => true,
            'comment' => '部门名称',
        ),
        'superior_id' => array(
            'type' => 'int(1)',
            'default' => 0,
            'comment' => '上级部门ID',
        ),
    ),
    'comment' => '部门列表',
);