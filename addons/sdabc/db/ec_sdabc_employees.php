<?php
/**
 * table: ims_ec_sdabc_employees
 */

defined('IN_IA') or exit('Access Denied!');

$db['ec_sdabc_employees'] = array(
    'columns' => array(
        'id' => array(
            'type' => 'int(11)',
            'required' => true,
            'auto_increment' => true,
            'primary' => true,
        ),
        'uniacid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'openid' => array(
            'type' => 'varchar(50)',
            'required' => true,
        ),
        'uid' => array(
            'type' => 'int(11)',
            'required' => true,
            'unique' => true,
            'comment' => '会员编号',
        ),
        'employee_id' => array(
            'type' => 'varchar(15)',
            'required' => true,
            'unique' => true,
            'comment' => '员工编号',
        ),
        'employee_name' => array(
            'type' => 'varchar(15)',
        ),
        'department_id' => array(
            'type' => 'int(1)',
            'comment' => '部门ID，departments 表外键',
        ),
        'read_count' => array(
            'type' => 'int(11)',
            'default' => 0,
            'comment' => '文章阅读数量',
        ),
        'fans_count' => array(
            'type' => 'int(11)',
            'default' => 0,
            'comment' => '拓展粉丝数量',
        ),
        'wechat_id' => array(
            'type' => 'varchar(50)',
            'comment' => '微信号',
        ),
        'created_time' => array(
            'type' => 'int(11)',
            'comment' => '员工加入时间',
        ),
    ),
    'comment' => '农行员工列表',
);