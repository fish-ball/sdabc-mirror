<?php
/**
 * ims_ec_sdabc_employees_fans
 */

defined('IN_IA') or exit('Access Denied!');


$db['ec_sdabc_employee_fans'] = array(
    'columns' => array(
        'id' => array(
            'type' => 'int(11)',
            'required' => true,
            'auto_increment' => true,
            'primary' => true,
        ),
        'uniacid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'subscription_uniacid' => array(
            'type' => 'int(11)',
            'required' => true,
            'comment' => '订阅号ID',
        ),
        'employee_uid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'fans_uid' => array(
            'type' => 'int(11)',
            'required' => true,
            'unique' => true,
        ),
        'is_follow' => array(
            'type' => 'tinyint(1)',
            'default' => 0,
            'comment' => '扫完员工二维码之后，粉丝是否关注订阅号。0，否；1，是。',
        ),
        'article_id' => array(
            'type' => 'int(11)',
            'comment' => '拓展粉丝的文章ID',
        ),
        'interact_time' => array(
            'type' => 'int(11)',
            'default' => 1,
            'comment' => '粉丝与平台互动次数',
        ),
        'created_time' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
    ),
    'comment' => '员工粉丝关系表',
);