<?php
/**
 * ims_ec_sdabc_employee_id
 */

defined('IN_IA') or exit('Access Denied!');

$db['ec_sdabc_employee_id'] = array(
    'columns' => array(
        'id' => array(
            'type' => 'int(11)',
            'required' => true,
            'primary' => true,
            'auto_increment' => true,
        ),
        'uniacid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'employee_id' => array(
            'type' => 'varchar(15)',
            'required' => true,
            'unique' => true,
        ),
        'employee_name' => array(
            'type' => 'varchar(25)',
            'required' => true,
        ),
    ),
    'comment' => '员工工号记录表',
);