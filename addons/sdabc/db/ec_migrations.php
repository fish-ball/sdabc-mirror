<?php
/**
 * table: ims_ec_migrations
 */

defined('IN_IA') or exit('Access Denied!');

$db['ec_migrations'] = array(
    'columns' => array(
        'id' => array(
            'type' => 'int(11)',
            'required' => true,
            'auto_increment' => true,
            'primary' => true,
        ),
        'table_name' => array(
            'type' => 'varchar(50)',
            'required' => true,
            'comment' => '表名',
        ),
        'table_data' => array(
            'type' => 'text',
            'required' => true,
            'comment' => '表内容',
        ),
        'created_time' => array(
            'type' => 'int(11)',
            'required' => true,
            'comment' => '创建时间',
        ),
    ),
    'comment' => '数据库更新记录表',
);