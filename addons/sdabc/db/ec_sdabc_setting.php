<?php
/**
 * ims_ec_sdabc_setting
 */

defined('IN_IA') or exit('Access Denied!');


$db['ec_sdabc_setting'] = array(
    'columns' => array(
        'id' => array(
            'type' => 'int(11)',
            'required' => true,
            'primary' => true,
            'auto_increment' => true,
        ),
        'uniacid' => array(
            'type' => 'int(11)',
            'required' => true,
        ),
        'subscription_uniacid' => array(
            'type' => 'int(11)',
            'comment' => '订阅号ID',
        ),
    ),
    'comment' => '模块设置',
);