<?php
/**
 * ims_ec_sdabc_employee_id
 */

defined('IN_IA') or exit('Access Denied!');


class EmployeeId extends EcModel
{
    public function __construct($condition)
    {
        parent::__construct('ec_sdabc_employee_id', $condition);
    }

    /**
     * Check the employee id and name
     *  + The employee id whether is legal
     *  + The employee id whether has been registered
     * @param $employee array array('employee_id' => xx, 'employee_name' => xx)
     * @return string
     */
    public function checkEmployeeId($employee)
    {
        // Check the $employee_id whether is a legal employee id
        $employee = $this->getList(array(
            "employee_id='{$employee['employee_id']}'",
            "employee_name='{$employee['employee_name']}'",
        ));
        if (empty($employee)) {
            return '请输入正确的工号和姓名！';
        }
        // Check the $employee_id whether has been registered
        if (!empty(ec_model('Employees')->getList(array("employee_id='{$employee['employee_id']}'")))) {
            return '该工号已经被注册了哦！';
        }
        return 'success';
    }

    /**
     * Filter out the employee id if it has been register
     * @param $ids array|string the employee id that want to insert
     * @return array
     */
    public function filterEmployeeId($ids)
    {
        is_array($ids) or $ids = array($ids);
        $exist_ids = array_map(function($item) {
            return $item['employee_id'];
        }, $this->getList());
        $no_exist = array();
        foreach ($ids as $id) {
            in_array($id['employee_id'], $exist_ids) or $no_exist[] = $id;
        }
        return $no_exist;
    }

    /**
     * Batch insert data
     * @param $ids array|string employee id
     */
    public function batchInsert($ids)
    {
        global $_W;
        if (!empty($ids = $this->filterEmployeeId($ids))) {
            $sql = "INSERT INTO " . tablename('ec_sdabc_employee_id') .
                " (`uniacid`, `employee_id`, `employee_name`) VALUES ";
            foreach ($ids as $id) {
                $sql .= "({$_W['uniacid']}, '{$id['employee_id']}', '{$id['employee_name']}'), ";
            }
            $sql = substr($sql, 0, strlen($sql) - 2);
            pdo_query($sql);
        }
    }
}