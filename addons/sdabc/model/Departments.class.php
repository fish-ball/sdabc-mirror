<?php
/**
 * ims_ec_sdabc_departments
 */

defined('IN_IA') or exit('Access Denied!');

class Departments extends EcModel
{
    public function __construct($condition = '')
    {
        parent::__construct('ec_sdabc_departments', $condition);
    }

    public function getList($params = array(), $other = '', $recombine_by = '')
    {
        $lists = parent::getList($params, $other);
        // recombine the list array
        if ($recombine_by) {
            $new_list = array();
            foreach ($lists as $list) {
                $new_list[$list[$recombine_by]] = $list;
            }
            $lists = $new_list;
        }
        return $lists;
    }

    /**
     * Delete the department and its subordinate department at the same time
     * @param $params
     * @return int|mixed
     */
    public function deleteRelation($params)
    {
        $departments = $this->getList();
        // Get the subordinate department id (the department of level 2)
        $subordinate_id = array();
        foreach ($departments as $item) {
            $item['superior_id'] == $params['id'] and $subordinate_id[] = $item['id'];
        }
        $subordinate_id[] = $params['id'];
        $params['id'] = $subordinate_id;
//        var_dump($params);
        return $this->delete($params);
    }

    /**
     * Sorting the department:
     *  the department of level 2 is next to the superior department (level 1)
     * @return array
     */
    public function departmentSort()
    {
        $departments = $this->getList();
        // Get the departments of level 1
        $superiors = array();
        foreach ($departments as $department) {
            $department['superior_id'] or $superiors[] = $department;
        }
        // Sorting the departments
        $sorting = array();
        foreach ($superiors as $superior) {
            $sorting[] = $superior;
            foreach ($departments as $item) {
                $superior['id'] == $item['superior_id'] and $sorting[] = $item;
            }
        }
        return $sorting;
    }

    /**
     * Get the subordinate department id
     * @param $department_id
     * @return array|bool false, if the $department_id is not legal
     *                          or the department is not a superior department
     *                          or the department has not subordinate department
     *                    array, the subordinate department id of department.
     */
    public function getSubordinateDepartmentId($department_id)
    {
        $departments = $this->getList();
        // Judge the id whether is department
        // and the department whether is superior department
        $is_department = false;
        $is_superior = false;
        foreach ($departments as $department) {
            if ($department['id'] == $department_id) {
                $is_department = true;
                if (intval($department['superior_id']) == 0) {
                    $is_superior = true;
                    break;
                }
            }
        }
        // If the the id is not department id
        // or the department is not superior department, return false
        if (!$is_department or !$is_superior) {
            return false;
        }
        // Get the surbodinate department id
        $subordinate_id = array();
        foreach ($departments as $department) {
            if ($department['superior_id'] == $department_id) {
                $subordinate_id[] = $department['id'];
            }
        }
        return empty($subordinate_id) ? false : $subordinate_id;
    }
}