<?php
/**
 * ims_ec_sdabc_setting
 */

defined('IN_IA') or exit('Access Denied!');


class Setting extends EcModel
{
    public function __construct($condition)
    {
        parent::__construct('ec_sdabc_setting', $condition);
    }
}