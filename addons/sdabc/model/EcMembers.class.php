<?php
/**
 * ims_mc_members
 */

defined('IN_IA') or exit('Access Denied!');


class EcMembers extends EcModel
{
    public function __construct($condition = '')
    {
        parent::__construct('mc_members', $condition);
    }

    /**
     * Get the members from table ims_mc_members
     * @param array $params SQL select condition e.g. array('id>=3', "name='lizs'")
     * @param string $other other SQL condition e.g. 'ORDER BY `name` DESC LIMIT 1'
     * @param bool $visual  true|false
     * @param string $recombine recombine data array with $recombine as the key
     * @param string $employee ''|'yes'|'no', filter the employee.
     *              'yes': the members is employee at the same time;
     *              'no': the members is not a employee at the same time;
     * @return array|mixed
     */
    public function getList($params = array(), $other = '', $visual = false,
                            $recombine = '', $employee = '')
    {
        $list = parent::getList($params, $other);
        // Visualization data list
        if ($visual) {
            foreach ($list as &$item) {
                // email
                $item['email'] = substr($item['email'], -6) == 'we7.cc' ? '' : $item['email'];
                // Gender: 0, 保密；1，男；2，女
                $item['gender_text'] = $item['gender'] == 1 ?
                    '男' :
                    ($item['gender'] == 2 ? '女' : '保密');
                // Address: province + city
                $item['addr'] = $item['resideprovince'] + $item['residecity'];
            }
            unset($item);
        }
        // Recombine data array with key $recombine
        if ($recombine) {
            $members = array();
            foreach ($list as $item) {
                $members[$item[$recombine]] = $item;
            }
            $list = $members;
        }
        // Filter employee
        if ($employee) {
            $employees = ec_model('Employees')->getList();
            $uids = array_map(function($item) {
                return $item['uid'];
            }, $employees);
            $new_list = array();
            // The member is employee at the same time
            if ($employee == 'yes') {
                foreach ($list as $k => $v) {
                    in_array($v['uid'], $uids) && $new_list[$k] = $v;
                }
            } else { // The member is not employee
                foreach ($list as $k => $v) {
                    !in_array($v['uid'], $uids) && $new_list[$k] = $v;
                }
            }
            $list = $new_list;
        }
        return $list;
    }

    /**
     * Judge the member whether is a new fans.
     *  According to, ims_mc_members.created_time is less than (time() - 10s)
     * @param $uid int member uid
     * @param bool $is_new true, the member must be a new fans
     * @param string $is_employee ''|'yes'|'no'
     *                          if 'yes', the member can be a employee
     *                          if 'no', the member can't be a employee
     * @return bool true, is a new fans; false, is not a new fans
     */
    public function isNewFans($uid, $is_new = false, $is_employee = 'no')
    {
        $is_fans = false;
        // Get the member, meanwhile the member is not a employee
        $member = $this->getList(array('uid=' . $uid), '', false, '', $is_employee);
        if (!empty($member)) {
            // If the current member is not a fans of a employee
            empty(ec_model('EmployeeFans')->getList(array('fans_uid=' . $uid))) and
            $is_fans = true and
            // If the member must be a new one
            $is_new and time() - $member[0]['createtime'] < 10 and $is_fans = false;
        }
        return $is_fans;
    }
}