<?php
/**
 * ims_ec_sdabc_employee_article
 */

defined('IN_IA') or exit('Access Denied!');


class EmployeeArticle extends EcModel
{
    public function __construct($condition = '')
    {
        parent::__construct('ec_sdabc_employee_article', $condition);
    }
}