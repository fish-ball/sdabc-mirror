<?php
/**
 * ims_ec_employee_fans
 */

defined('IN_IA') or exit('Access Denied!');


class EmployeeFans extends EcModel
{
    public function __construct($condition = '')
    {
        parent::__construct('ec_sdabc_employee_fans', $condition);
    }

    public function statistics($params = array())
    {
        global $_W;
        // First find out the employee
        $filter = array();
        if ($params['department_id']) {
            $subordinate = ec_model('Departments')->getSubordinateDepartmentId($params['department_id']);
            if ($subordinate) {
                $sql = implode(', ', $subordinate);
                $filter[] = "department_id IN ({$sql})";
            } else {
                $filter[] = "department_id={$params['department_id']}";
            }
        }
        $params['employee_id'] and $filter[] = "employee_id='{$params['employee_id']}'";
        $params['employee_name'] and $filter[] = "employee_name='{$params['employee_name']}'";
        $employees = ec_model('Employees')->getList($filter);
        // Finding out the fans of the employees
        if (!empty($employees)) {
            $fans_uniacid = pdo_getcolumn(
                'ec_sdabc_setting',
                array('uniacid' => $_W['uniacid']),
                'subscription_uniacid'
            );
            $fans = pdo_getall('mc_members', array('uniacid' => $fans_uniacid));
            $employee_fans = $this->getList(array(
                "created_time>={$params['start_time']}",
                "created_time<={$params['end_time']}",
                "subscription_uniacid={$fans_uniacid}",
                "is_follow=1",
            ));
            // 本次统计粉丝总数量
            $all_fans_count = 0;
            foreach ($employees as &$employee) {
                $fans_uid = array();
                foreach ($employee_fans as $item) {
                    if ($item['employee_uid'] == $employee['uid']) {
                        $fans_uid[] = $item['fans_uid'];
                    }
                }
                // The fans of the employee
                $employee['fans'] =array();
                foreach ($fans as $fan) {
                    if (in_array($fan['uid'], $fans_uid)) {
                        $employee['fans'][] = $fan;
                        $all_fans_count++;
                    }
                }
            }
            unset($employee);
        }

        return array(
            'all_fans_count' => isset($all_fans_count) ? $all_fans_count : 0,
            'employees' => $employees,
        );
    }
}