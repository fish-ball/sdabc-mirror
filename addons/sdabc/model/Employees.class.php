<?php
/**
 * ims_ec_sdabc_employees
 */

defined('IN_IA') or exit('Access Denied!');


class Employees extends EcModel
{
    /**
     * Employees constructor.
     * @param string $condition SQL condition that select the one
     * @param bool $mc_member whether get the msg from ims_mc_members
     */
    public function __construct($condition = '', $mc_member = false)
    {
        parent::__construct('ec_sdabc_employees', $condition);
        // Get message from table ims_mc_members
        $mc_member and
        $this->item['mc'] = ec_model('EcMembers', array("uid={$this->item['uid']}"))->item;
    }

    public function getList($params = array(), $other = '', $mc_members = false)
    {
        $list = parent::getList($params, $other);

        // Get the department name
        $departments = ec_model('Departments')->getList(array(), '', 'id');
        foreach ($list as &$item) {
            $department = $departments[$item['department_id']];
            $superior_department = $department['superior_id'] ?
                $departments[$department['superior_id']]['name'] . ' - ' : '';
            $item['department'] = $superior_department . $department['name'];
        }
        unset($item);

        // Get message from table ims_mc_members
        if ($mc_members) {
            $members = ec_model('EcMembers')->getList(array(), '', true, 'uid');
            foreach ($list as &$item) {
                $item['mc'] = $members[$item['uid']];
            }
            unset($item);
        }

        // Real fans count
        $real_fans = true;
        if ($real_fans) {
            $fans = ec_model('EmployeeFans')->getList(array("is_follow=1"));
            $fans_counts = array();
            foreach ($fans as $fan) {
                $fans_counts[$fan['employee_uid']] += 1;
            }
            foreach ($list as &$item) {
                $item['real_fans_count'] = $fans_counts[$item['uid']] ?: 0;
            }
            unset($item);
        }
        return $list;
    }
}