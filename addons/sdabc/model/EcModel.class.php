<?php
/**
 * common model
 */

defined('IN_IA') or exit('Access Denied!');


class EcModel
{
    public $table;
    public $item;

    /**
     * EcModel constructor.
     * @param $table_name string the name of table, "ims_{$table_name}"
     * @param $condition string condition, e.g. "id=12", "name='xxx'" etc.
     */
    public function __construct($table_name, $condition = '')
    {
        global $_W;
        $table_name and $this->table = $table_name;
        $condition and $this->item = pdo_fetch(
            "SELECT * FROM " . tablename($table_name) .
            " WHERE uniacid={$_W['uniacid']} AND {$condition}"
        );
    }

    public function __get($name)
    {
        return $this->item[$name];
    }

    public function __set($name, $value)
    {
        pdo_update($this->table, array($name => $value), array('id' => $this->item['id']));
    }

    /**
     * Insert a row to table
     * @param $data array table data
     * @return int insert id
     */
    public function insert($data)
    {
        global $_W;
        if (!empty($data) && is_array($data)) {
            $data['uniacid'] = $_W['uniacid'];
            return pdo_insert($this->table, $data) ? pdo_insertid() : 0;
        }
        return 0;
    }

    /**
     * Delete data from table
     * @param $params
     * @return mixed|int return the number of delete count if success
     */
    public function delete($params)
    {
        global $_W;
        if (!empty($params) && is_array($params)) {
            $params['uniacid'] = $_W['uniacid'];
            return pdo_delete($this->table, $params);
        }
        return 0;
    }

    /**
     * Update table
     * @param array $data
     * @param array $filter
     * @return int|mixed return the row count of update if success,
     *                   0 if nothing change and false when something wrong
     */
    public function update($data, $filter = array())
    {
        global $_W;
        if (!empty($data) && is_array($data)) {
            $filter['uniacid'] = $_W['uniacid'];
            return pdo_update($this->table, $data, $filter);
        }
        return 0;
    }

    /**
     * Insert if it does not exist, otherwise update
     * @param $data
     * @param array $filter
     */
    public function insert_or_update($data, $filter = array())
    {
        if (!empty($data) && is_array($data)) {
            empty($this->getList($filter)) ?
                $this->insert($data) :
                $this->update($data, $filter);
        }
    }

    /**
     * Get the list of table
     * @param array $params SQL select condition e.g. array('id>=3', "name='lizs'")
     * @param string $other
     * @return mixed
     */
    public function getList($params = array(), $other = '')
    {
        global $_W;
        $params = is_array($params) ? $params : array($params);
        $params[] = "uniacid={$_W['uniacid']}";
        $condition = implode(' AND ', $params);
        $list = pdo_fetchall(
            "SELECT * FROM " . tablename($this->table) .
            " WHERE ${condition} {$other}"
        );
        return $list;
    }
}