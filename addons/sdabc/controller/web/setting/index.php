<?php

defined('IN_IA') or exit('Access Denied!');


class web_setting_index extends Core
{

    public function _context()
    {
        $context = array();
        $setting = ec_model('Setting')->getList();
        $context['setting'] = empty($setting) ? '' : $setting[0];
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        $this->template('web/setting/index', $context);
    }

    /**
     * Save setting
     */
    public function setting()
    {
        global $_GPC;
        checksubmit('submit') and ec_model('Setting')->insert_or_update($_GPC['setting']);
        $this->index();
    }
}