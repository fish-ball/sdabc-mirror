<?php

defined('IN_IA') or exit('Access Denied!');


class web_statistics_index extends Core
{
    public function _context()
    {
        $context = array();
        $context['departments'] = ec_model('Departments')->getList();
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        return $this->template('web/statistics/index', $context);
    }

    public function statistics()
    {
        global $_GPC;
        $context = $this->_context();
        if ($_GPC['statistics']) {
            $filter = array();
            $superior = intval($_GPC['superior']);
            $subordinate = intval($_GPC['subordinate']);
            $employee_id = strval(trim($_GPC['employee_id']));
            $employee_name = strval(trim($_GPC['employee_name']));
            $start_time = strtotime($_GPC['time']['start']);
            $end_time = strtotime($_GPC['time']['end']);
            // department id
            $filter['department_id'] = $subordinate ?: $superior;
            $filter['employee_id'] = $employee_id;
            $filter['employee_name'] = $employee_name;
            $filter['start_time'] = $start_time;
            $filter['end_time'] = $end_time + 3600 * 24;
            $context['result'] = ec_model('EmployeeFans')->statistics($filter);
            $context['superior'] = $superior;
            $context['subordinate'] = $subordinate;
            $context['employee_id'] = $employee_id;
            $context['employee_name'] = $employee_name;
            $context['start_time'] = $start_time;
            $context['end_time'] = $end_time;
        }
        return $this->template('web/statistics/index', $context);
    }
}
