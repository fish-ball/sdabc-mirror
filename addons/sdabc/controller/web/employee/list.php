<?php

defined('IN_IA') or exit('Access Denied!');


/**
 * employee list
 * Class web_employee_list
 */
class web_employee_list extends Core
{
    public $employees;

    public function __construct()
    {
        $this->menus = $this->_employeeMenus();
        $this->employees = $this->_model('Employees');
    }

    /**
     * 【员工列表】
     * @return mixed|string
     */
    public function index()
    {
        $context = array();
        $context['employees'] = $this->employees->getList(array(), '', true);
        return $this->template('web/employee/list', $context);
    }

    /**
     * Delete employee
     */
    public function delete()
    {
        global $_GPC;
        if ($_GPC['action'] == 'delete' && $id = $_GPC['id']) {
            $delete = $this->employees->delete(array('id' => $id));
            echo $delete > 0 ? 'success' : 'failed';
        }
    }
}