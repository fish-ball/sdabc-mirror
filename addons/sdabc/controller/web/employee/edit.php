<?php

defined('IN_IA') or exit('Access Denied!');


class web_employee_edit extends Core
{
    public function __construct()
    {
        global $_GPC;
        $this->menus = $this->_employeeMenus();
        array_splice($this->menus, 1, 0, array(
            array(
                'text' => '编辑员工',
                'url' => $this->wUrl(
                    'employee/web_employee_edit/index',
                    array('uid' => intval($_GPC['uid']))
                ),
                'active' => 'web_employee_edit',
                ),
        ));
    }

    public function _context()
    {
        global $_GPC;
        $context = array();
        $uid = intval($_GPC['uid']);
        // $context['employee'] = ec_model('Employees', array("uid={$uid}", true))->item;
        $employee = ec_model('Employees')->getList(array("uid={$uid}"), '', true);
        $context['employee'] = empty($employee) ? '' : $employee[0];
        $context['qrcode_url'] = $this->mUrl(
            'qrcode/mobile_employee_qrcode/bindFans',
            array('employee_uid' => $uid),
            true,
            true
        );
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        return $this->template('web/employee/edit', $context);
    }

    /**
     * Update employee data
     */
    public function update()
    {
        global $_GPC;
        if (checksubmit('submit')) {
            ec_model('Employees')->update($_GPC['employee'], array(
                'id' => intval($_GPC['id']),
            )) !== false ? message('更新成功！', referer(), 'success') :
                 message('更新失败！', referer(), 'error');
        }
    }
}