<?php

defined('IN_IA') or exit('Access Denied!');


class web_employee_add extends Core
{
    public function __construct()
    {
        $this->menus = $this->_employeeMenus();
    }

    public function _context()
    {
        $context = array();
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        return $this->template('web/employee/add', $context);
    }

    /**
     * Search members
     * @return mixed|string
     */
    public function search()
    {
        global $_GPC;
        $context = $this->_context();
        // Submit search
        if ($_GPC['search_submit'] == 'search') {
            $context['search_key'] = $_GPC['search_key'];
            // search condition
            $params = array();
            if ($val = trim($_GPC['search_value'])) {
                $context['search_value'] = $val;
                if ($_GPC['search_key'] == 'uid') {
                    $params = array("uid={$val}");
                } else {
                    $params = array("{$_GPC['search_key']} LIKE '%{$val}%'");
                }
            }
            $context['members'] = ec_model('EcMembers')->getList($params, '', true, '', 'no');
        }
        return $this->template('web/employee/add', $context);
    }

    /**
     * Add employee
     */
    public function add()
    {
        global $_GPC;
        if ($_GPC['action'] == 'add' && $uid = $_GPC['uid']) {
            $data = array(
                'uid' => $uid,
                'created_time' => time(),
            );
            // If the new employee has been an employee before,
            // recover the read count and fans count
            $read_count = ec_model('EmployeeArticle')->getList(array('employee_uid=' . $uid));
            empty($read_count) or $data['read_count'] = sizeof($read_count);
            $fans_count = ec_model('EmployeeFans')->getList(array('employee_uid=' . $uid));
            empty($fans_count) or $data['fans_count'] = sizeof($fans_count);
            // Insert an employee
            $insert_id = ec_model('Employees')->insert($data);
            echo $insert_id > 0 ? 'success' : 'failed';
        }
    }
}