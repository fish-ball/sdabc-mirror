<?php

defined('IN_IA') or exit('Access Denied!');


class web_employee_id extends Core
{
    public function __construct()
    {
        $this->menus = $this->_employeeMenus();
    }

    public function _context()
    {
        $context = array();
        $context['employee_ids'] = ec_model('EmployeeId')->getList(array(), "ORDER BY `id`");
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        return $this->template('web/employee/id', $context);
    }

    /**
     * Add employee id
     * @return mixed|string
     */
    public function add()
    {
        global $_GPC;
        if (checksubmit('submit')) {
            $input_type = $_GPC['input_type'];
            $employeeId = ec_model('EmployeeId');
            // Add employee id manually
            if ($input_type == 'manual' and
                $employee_id = strval(trim($_GPC['employee_id'])) and
                $employee_name = strval(trim($_GPC['employee_name']))) {
                $employeeId->batchInsert(array(
                    array(
                        'employee_id' => $employee_id,
                        'employee_name' => $employee_name,
                    ),
                ));
            }
            // Add employee id by excel
            if ($input_type == 'excel' && $excel_file = $_FILES['excel']) {
            }
        }
        $this->index();
    }

    /**
     * Delete employee id
     */
    public function delete()
    {
        global $_GPC;
        if (!empty($ids = $_GPC['employee_id'])) {
            $res = ec_model('EmployeeId')->delete(array('employee_id' => $ids));
            echo $res ? 'success' : 'failed';
        }
    }
}