<?php

defined('IN_IA') or exit('Access Denied!');


class web_department_add extends Core
{
    public function __construct()
    {
        $this->menus = $this->_departmentMenus();
    }

    public function _context()
    {
        $context = array();
        $context['departments'] = ec_model('Departments')->getList('superior_id=0');
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        return $this->template('web/department/add', $context);
    }

    /**
     * Add department
     */
    public function add()
    {
        global $_GPC;
        if (checksubmit('submit') && $name = strval(trim($_GPC['department_name']))) {
            $data['name'] = $name;
            $superior_id = intval($_GPC['superior_id']) and $data['superior_id'] = $superior_id;
            $departmentObj = ec_model('Departments');
            // if the department doesn't exist
            if (empty($departmentObj->getList(array("name='{$name}'")))) {
                $departmentObj->insert($data) ?
                    message('添加成功！', referer(), 'success') :
                    message('添加失败！', referer(), 'error');
            } else {
                message("{$name} 已经存在", referer(), 'error');
            }
        }
        $this->index();
    }
}