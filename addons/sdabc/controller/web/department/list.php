<?php

defined('IN_IA') or exit('Access Denied!');


/**
 * employee list
 * Class web_employee_list
 */
class web_department_list extends Core
{
    public $employees;

    public function __construct()
    {
        $this->menus = $this->_departmentMenus();
    }

    /**
     * 【部门列表】
     * @return mixed|string
     */
    public function index()
    {
        $context = array();
        $context['departments'] = ec_model('Departments')->departmentSort();
        return $this->template('web/department/list', $context);
    }

    /**
     * Delete department
     */
    public function delete()
    {
        global $_GPC;
        if ($_GPC['action'] == 'delete' && $id = intval($_GPC['department_id'])) {
            $delete = ec_model('Departments')->deleteRelation(array('id' => $id));
            echo $delete > 0 ? 'success' : 'failed';
        }
    }

    /**
     * Save the edit
     */
    public function save()
    {
        global $_GPC;
        if ($_GPC['action'] == 'save' and $id = intval($_GPC['department_id']) and
                $name = strval(trim($_GPC['department_name']))) {
            ec_model('Departments')->update(array('name' => $name), array('id' => $id));
        }
    }
}