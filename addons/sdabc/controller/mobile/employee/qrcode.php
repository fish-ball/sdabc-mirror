<?php

defined('IN_IA') or exit('Access Denied!');


class mobile_employee_qrcode extends Core
{
    public function __construct()
    {
    }

    public function index()
    {
        global $_W;
        $employee_uid = $_W['member']['uid'];
        $context = array();
        empty($employee = ec_model('Employees')->getList(array("uid=" . $employee_uid))) or
        $context['qrcode_url'] = $this->mUrl(
            'qrcode/mobile_employee_qrcode/bindFans',
            array('employee_uid' => $employee_uid),
            true,
            true
        );
        $context['qrcode_type'] = 'employee';
        return $this->template('mobile/employee/qrcode', $context);
    }

    /**
     * 绑定员工与粉丝关系：
     *  + 粉丝第一次绑定，即之前不能够被员工绑定过
     */
    public function bindFans()
    {
        // 先检查平台有没有客户信息，如果没有就通过网页授权方式获取用户信息
        checkauth();
        global $_GPC, $_W;
        $context = array();
        $data = array();
        // 如果粉丝信息获取成功，将员工和粉丝关系绑定
        if ($data['fans_uid'] = intval($_W['member']['uid'])) {
            $employee_fans = ec_model('EmployeeFans');
            $setting = ec_model('Setting')->getList();
            $setting = empty($setting) ? false : $setting[0];
            if ($setting and $sub_id = $setting['subscription_uniacid']) {
                $no_bind = empty($employee_fans->getList(array(
                    "fans_uid={$data['fans_uid']}",
                    "subscription_uniacid=" . $sub_id,
                )));
                // If the fan hasn't bound with certain employee before
                if ($no_bind) {
                    $data['employee_uid'] = $_GPC['employee_uid'];
                    $data['subscription_uniacid'] = $setting ? $sub_id : 0;
                    $data['created_time'] = time();
                    $employee_fans->insert($data);
                }
                // 获取订阅号二维码
                $context['qrcode_url'] = tomedia("qrcode_{$sub_id}.jpg");
            }
        }
        $context['qrcode_type'] = 'subscription';
        return $this->template('mobile/employee/qrcode', $context);
    }
}