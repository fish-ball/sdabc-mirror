<?php
/**
 * 【员工注册】
 */

defined('IN_IA') or exit('Access Denied!');


class mobile_employee_register extends Core
{
    public function __construct()
    {
    }

    public function _context()
    {
        global $_W;
        $context = array();
        // $_W['member']['uid'] = 3;
        $employee = ec_model('Employees')->getList(
            array('uid=' . $_W['member']['uid'])
        );
        $context['employee'] = empty($employee) ? '' : $employee[0];
        $context['departments'] = ec_model('Departments')->getList();
        // $context['register_result'] = '该工号已经被注册了哦！';
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        $context['form_url'] = empty($context['employee']) ?
            $this->mUrl('register/mobile_employee_register/register') :
            $this->mUrl('register/mobile_employee_register/update');
        return $this->template('mobile/employee/register', $context);
    }

    /**
     * Verify the employee id and name
     */
    public function verify()
    {
        global $_GPC;
        if ($_GPC['action'] == 'verify' and
            $employee_id = strval($_GPC['employee_id']) and
            $employee_name = strval($_GPC['employee_name'])) {
            $verify_result = ec_model('EmployeeId')->checkEmployeeId(array(
                'employee_id' => $employee_id,
                'employee_name' => $employee_name,
            ));
            // Return the verify result
            if ($verify_result != 'success') {
                $result = array(
                    'status' => 'error',
                    'content' => $verify_result,
                );
            } else {
                $result = array(
                    'status' => 'success',
                    'content' => '工号和姓名正确，请选择相应的部门！',
                );
            }
            echo json_encode($result);
        }
    }

    /**
     * Employee register
     * @return mixed|string
     */
    public function register()
    {
        global $_GPC, $_W;
        // $_W['member']['uid'] = 3;
        if (checksubmit('submit')) {
            $employee = $_GPC['employee'];
            if ($employee_id = strval(trim($employee['employee_id'])) and
                $employee_name = strval(trim($employee['employee_name'])) and
                $department = intval($_GPC['superior_department'])) {
                // Checking employee id  and name
                $verify_result = ec_model('EmployeeId')->checkEmployeeId(array(
                    'employee_id' => $employee_id,
                    'employee_name' => $employee_name,
                ));
                if ($verify_result == 'success') {
                    $employee['uid'] = $_W['member']['uid'];
                    $employee['department_id'] = intval($_GPC['subordinate_department']) ?: $department;
                    $employee['created_time'] = time();
                    ec_model('Employees')->insert($employee);
                } else {
                    $result = $verify_result;
                }
            }
        }
        $context = $this->_context();
        isset($result) and $context['register_result'] = $result;
        return $this->template('mobile/employee/register', $context);
    }

}