<?php
/**
 * Employee transmit article
 */


class mobile_employee_transmit extends Core
{
    /**
     * When the article is transmitted
     *  + If the viewer is a new fans
     *  + If the viewer is a employee
     */
    public function transmit()
    {
        global $_GPC;
        $is_transmit = intval($_GPC['is_transmit']);
        $employee_uid = intval($_GPC['employee_uid']);
        $member_uid = intval($_GPC['member_uid']);
        $article_id = intval($_GPC['article_id']);

        // If current member is a employee,
        // add a reading article record and return the uid to ajax request.
        $member = ec_model('Employees')->getList(array('uid=' . $member_uid));
        if (!empty($member)) {
            $readObj = ec_model('EmployeeArticle');
            if ($article_id) {
                $read_record = $readObj->getList(array(
                    'employee_uid=' . $member_uid,
                    'article_id=' . $article_id
                ));
                empty($read_record) and
                $readObj->insert(array(
                    'employee_uid' => $member_uid,
                    'article_id' => $article_id,
                    'created_time' => time(),
                )) and
                ec_model('Employees', array('uid=' . $member_uid))->read_count += 1;
            }
            // Return the employee uid to ajax request
            echo $member_uid;
        }

        // The article has been transmitted
        if ($is_transmit > 0 && $employee_uid && $member_uid && $employee_uid != $member_uid) {
            // If the current member is a new fans, binding the relationship
            if (ec_model('EcMembers')->isNewFans($member_uid)) {
                $insert_id = ec_model('EmployeeFans')->insert(array(
                    'employee_uid' => $employee_uid,
                    'fans_uid' => $member_uid,
                    'article_id' => $article_id,
                    'created_time' => time(),
                ));
                // The employee fans_count + 1
                $insert_id and
                ec_model('Employees', array('uid=' . $employee_uid))->fans_count += 1;
            }
        }
    }

    /**
     * Expand fans by employee qrcode
     */
    public function employeeQrcode()
    {
        global $_GPC, $_W;
        $is_fan = false;
        $employee_uid = intval($_GPC['employee_uid']);
        $member_uid = intval($_W['member']['uid']);
        if ($employee_uid && $member_uid && ec_model('EcMembers')->isNewFans($member_uid)) {
            $insert_id = ec_model('EmployeeFans')->insert(array(
                'employee_uid' => $employee_uid,
                'fans_uid' => $member_uid,
                'created_time' => time(),
            ));
            // The employee fans_count + 1
            $insert_id and
            ec_model('Employees', array('uid=' . $employee_uid))->fans_count += 1;
            $is_fan = true;
        }
        header("Location: " . murl('home//', array(), true, true));
    }
}