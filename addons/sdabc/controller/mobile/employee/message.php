<?php
/**
 * Employee message: e.g. id, name, department, fans count
 */

defined('IN_IA') or exit('Access Denied!');


class mobile_employee_message extends Core
{
    public function _context()
    {
        global $_W;
        $context = array();
        $employee = ec_model('Employees')->getList(array("uid={$_W['member']['uid']}"), '', true);
        if (!empty($employee)) {
            $context['employee'] = $employee[0];
            $departments = ec_model('Departments')->getList();
            foreach ($departments as $department) {
                // Find out the department of the employee
                if ($department['id'] = $employee[0]['department_id']) {
                    $context['superior_id'] = $department['superior_id'] ?: 0;
                    $context['subordinate_id'] = $department['superior_id'] ? $department['id'] : 0;
                }
            }
        }
        $context['employee'] = empty($employee) ? false : $employee[0];
        $context['departments'] = ec_model('Departments')->getList();
        return $context;
    }

    public function index()
    {
        $context = $this->_context();
        return $this->template('mobile/employee/message', $context);
    }

    /**
     * Update employee department
     */
    public function updateDepartment()
    {
        global $_GPC, $_W;
        if (checksubmit('update_department')) {
            $superior_id = intval($_GPC['superior_id']) ?: 0;
            $subordinate_id = intval($_GPC['subordinate_id']) ?: 0;
            $employee['department_id'] = $subordinate_id ?: $superior_id;
            ec_model('Employees')->update($employee, array(
                'uid' => $_W['member']['uid'],
            ));
        }
        $this->index();
    }

    /**
     * Check the number of fans that the employee extend
     */
    public function checkFansCount()
    {
        global $_GPC, $_W;
        if ($_GPC['action'] == 'check_fans_count') {
            $from_date = strtotime(strval(trim($_GPC['from_date'])));
            $to_date = strtotime(strval(trim($_GPC['to_date'])));
            $check_result = ec_model('EmployeeFans')->getList(array(
                "created_time>={$from_date}",
                "created_time<={$to_date}",
                "employee_uid={$_W['member']['uid']}",
                "is_follow=1"
            ));
            echo count($check_result);
        }
    }
}