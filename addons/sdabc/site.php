<?php
/**
 * 员工管理模块微站定义
 *
 * @author easecloud
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');

// Module autoload
require_once "core/autoload.php";

class SdabcModuleSite extends Core {

    /**
     * 后台【员工管理】入口
     */
	public function doWebEmployee()
    {
        $this->_route('web_employee_list/index');
	}

    /**
     * 后台【部门管理】入口
     */
    public function doWebDepartment()
    {
        $this->_route('web_department_list/index');
    }

    /**
     * 后台【统计】入口
     */
    public function doWebStatistics()
    {
        $this->_route('web_statistics_index/index');
    }

    /**
     * 后台【设置】入口
     */
    public function doWebSetting()
    {
        $this->_route('web_setting_index/index');
    }

    /**
     * 手机端【员工注册】入口
     */
	public function doMobileRegister()
    {
        $this->_route('mobile_employee_register/index');
    }

    /**
     * 手机端【员工信息】入口
     */
    public function doMobileMessage()
    {
        $this->_route('mobile_employee_message/index');
    }

    /**
     * 【员工二维码】
     */
    public function doMobileQrcode()
    {
        $this->_route('mobile_employee_qrcode/index');
    }

    /**
     * 手机端转发文章拓展粉丝逻辑处理
     */
    public function doMobileTransmit()
    {
        $this->_route('mobile_employee_transmit/transmit');
    }

    /**
     * 【操作文档】入口
     */
    public function doWebDocument()
    {
        global $_W;
        header("Location: " . $_W['siteroot'] . 'doc/');
    }

}