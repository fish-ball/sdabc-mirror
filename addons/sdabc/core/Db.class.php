<?php
defined('IN_IA') or exit('Access Denied!');

/**
 * Class EcDb
 * $db array 模块所有数据库表单数据
 */
class EcDb
{
    public $db;

    /**
     * EcDb constructor.
     * 获取所有数据库表单数据，并创建表 ims_ec_migrations
     */
    public function __construct()
    {
        global $_W;
        $db = array();
        // Require database file
        $db_files = ec_get_files(SDABC_DB);
        foreach ($db_files as $file) {
            require_once $file;
        }
        // Create migrations table
        $migration_name = 'ec_migrations';
        if (!pdo_tableexists($migration_name)) {
            pdo_query($this->generateTableSql(
                $_W['config']['db']['tablepre'] . $migration_name,
                $db[$migration_name])
            );
        }
        $this->db = $db;
    }

    /**
     * 过滤 SQL 语句
     * @param $data
     * @return mixed
     */
    public function filterTableData($data)
    {
        $unique = array();
        foreach ($data['columns'] as $k => $v) {
            // 类型
            $columns['type'] = $v['type'];
            // 排序规则
            $columns['charset'] = @$v['charset'] ?: '';
            // 是否允许为空
            $columns['required'] = @$v['required'] ? 'NOT NULL' : '';
            // 默认值
            $columns['default'] = isset($v['default']) ? 'DEFAULT ' . $v['default'] : '';
            // 列注释
            $columns['comment'] = isset($v['comment']) ? "COMMENT '{$v['comment']}'" : '';
            // 自增
            $columns['auto_increment'] = @$v['auto_increment'] ? 'AUTO_INCREMENT' : '';
            // 主键
            $columns['primary'] = @$v['primary'] ? 'PRIMARY KEY' : '';
            @$v['unique'] and $unique[] = "`{$k}`";
            $data['columns'][$k] = $columns;
        }
        $data['unique'] = $unique;
        // 表注释
        $data['comment'] = @$data['comment'] ? "COMMENT='{$data['comment']}'" : '';
        $data['engine'] = @$data['engine'] ?: 'MyISAM';
        $data['charset'] = @$data['charset'] ?: 'utf8';
        return $data;
    }

    /**
     * 生成表的 SQL 语句
     * @param $table_name
     * @param $data
     * @return string
     */
    public function generateTableSql($table_name, $data)
    {
        $data = $this->filterTableData($data);
        $columns = array();
        foreach ($data['columns'] as $k => $v) {
            $columns[] = "`{$k}` {$v['type']} {$v['charset']} {$v['required']}
                    {$v['default']} {$v['comment']} {$v['primary']} {$v['auto_increment']}";
        }
        $column_sql = implode(',', $columns);
        if ($data['unique']) {
            $unique_key = implode(',', $data['unique']);
            $column_sql .= ", UNIQUE ({$unique_key})";
        }
        $sql = <<<SQL
CREATE TABLE `{$table_name}` (
{$column_sql}
) ENGINE={$data['engine']} DEFAULT CHARSET={$data['charset']} {$data['comment']}
SQL;
        return $sql;
    }

    /**
     * 更新数据库，添加列
     * @param $name string 数据库表名
     * @param $columns array 表中的列数据
     */
    private function addColumns($name, $columns)
    {
        $data['columns'] = $columns;
        $data = $this->filterTableData($data);
        foreach ($data['columns'] as $k => $v) {
            $sql = <<<SQL
ALTER TABLE `{$name}`
ADD COLUMN `{$k}` {$v['type']} {$v['charset']} {$v['required']}
{$v['default']} {$v['comment']} {$v['primary']} {$v['auto_increment']}
SQL;
            $status = pdo_query($sql) ? 'successful' : 'failed';
            echo "It's {$status} to add column: {$k} \n";
        }
    }

    /**
     * 更新数据库，删除列
     * @param $name string 数据库表名
     * @param $columns array 表中的列数据
     */
    private function deleteColumns($name, $columns)
    {
        foreach ($columns as $k => $v) {
            $sql = <<<SQL
ALTER TABLE `{$name}`
DROP COLUMN {$k}
SQL;
            $status = pdo_query($sql) ? 'successful' : 'failed';
            echo "It's {$status} to delete column: {$k} \n";
        }
    }

    /**
     * 数据库迁移：添加/删除列，TODO：更改列属性，更改表属性
     * @param $name string 表名
     * @param $data array 表数据
     */
    private function migrate($name, $data)
    {
        $migration = pdo_fetch(
            "SELECT * FROM " . tablename('ec_migrations') .
            " WHERE `table_name`='{$name}' ORDER BY created_time DESC LIMIT 1 "
        );
        $last_data = unserialize($migration['table_data']);
        // 找出新添加的列，并添加到数据库
        $new_columns = array_diff_key($data['columns'], $last_data['columns']);
        empty($new_columns) or $this->addColumns($name, $new_columns);
        // 找出删除的列，并从数据库中删除
        $deleted_columns = array_diff_key($last_data['columns'], $data['columns']);
        empty($deleted_columns) or $this->deleteColumns($name, $deleted_columns);
        // 添加数据库迁移记录
        if ($new_columns || $deleted_columns) {
            pdo_insert('ec_migrations', array(
                'table_name' => $name,
                'table_data' => serialize($data),
                'created_time' => time(),
            ));
        }
    }

    /**
     * 创建/更新数据库
     */
    public function update()
    {
        global $_W;
        foreach ($this->db as $k => $v) {
            // 如果原来没有的表，添加新表
            if (!pdo_tableexists($k)) {
                $k = $_W['config']['db']['tablepre'] . $k;
                $sql = $this->generateTableSql($k, $v);
                pdo_query($sql);
                echo "It's successful to create table: {$k} \n";
                // 迁移记录
                pdo_insert('ec_migrations', array(
                    'table_name' => $k,
                    'table_data' => serialize($v),
                    'created_time' => time(),
                ));
            } else { // 更新已有表
                $k = $_W['config']['db']['tablepre'] . $k;
                $this->migrate($k, $v);
            }
        }
    }

}