<?php
/**
 * module config
 */

defined('IN_IA') or exit('Access Denied!');

defined('SDABC_ROOT') or define('SDABC_ROOT', IA_ROOT . '/addons/sdabc/');
defined('SDABC_CORE') or define('SDABC_CORE', SDABC_ROOT . 'core/');
defined('SDABC_DB') or define('SDABC_DB', SDABC_ROOT . 'db/');
defined('SDABC_CTL') or define('SDABC_CTL', SDABC_ROOT . 'controller/');
defined('SDABC_MODEL') or define('SDABC_MODEL', SDABC_ROOT . 'model/');
defined('SDABC_TPL') or define('SDABC_TPL', SDABC_ROOT . 'template/');
defined('SDABC_STATIC') or define('SDABC_STATIC', SDABC_ROOT . 'static/');
