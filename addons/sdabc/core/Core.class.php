<?php
/**
 * module core
 */

defined('IN_IA') or exit('Access Denied!');


class Core extends WeModuleSite
{
    public $modulename = 'sdabc';
    // Module menus
    public $menus;

    /**
     * Route to the controller
     * @param $segment string $cn/$cm, $cn: class name; $cm: class method
     */
    public function _route($segment)
    {
        global $_GPC;
        list($cn, $cm) = explode('/', $segment);
        $cn = trim($_GPC['cn']) ?: $cn;
        $cm = trim($_GPC['cm']) ?: $cm;
        // Require and instance controller class
        $cls = ec_controller($cn);
        method_exists($cls, $cm) or message('未找到类方法：' . $cm, referer(), 'error');
        $cls->$cm();
    }

    public function _routeDoc()
    {
        header('');
    }

    /**
     * Require and instance model class
     * @param string $cn    the name of model class,
     *          note: the file name must be the same as class name
     * @param string $param the param when instance class
     * @return object
     */
    public function _model($cn, $param = '')
    {
        $file = SDABC_MODEL . "{$cn}.class.php";
        is_file($file) ?
            require_once $file :
            message('未找到模型类文件：' . $file, referer(), 'error');
        class_exists($cn) or message('模型类不存在：' . $cn, referer(), 'error');
        $cls = new ReflectionClass($cn);
        return $cls->newInstanceArgs(array($param));
    }

    /**
     * template
     * @param $file_path string the file path in template and exclude suffix,
     *          e.g. 'template/web/index.html' => 'web/index'
     * @param $context array
     * @return mixed|string
     */
    protected function template($file_path, $context = array())
    {
        global $_W;
        $name = $this->modulename;
        if(defined('IN_SYS')) {
            // Module template file
            $source = SDABC_TPL . "{$file_path}.html";
            // System template file
            is_file($source) or $source = IA_ROOT . "/develop/web/themes/{$_W['template']}/{$name}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/develop/web/themes/default/{$name}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/develop/web/themes/{$_W['template']}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/develop/web/themes/default/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/web/themes/{$_W['template']}/{$name}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/web/themes/default/{$name}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/web/themes/{$_W['template']}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/web/themes/default/{$file_path}.html";
            // Cache file path
            $compile = IA_ROOT . "/data/tpl/web/{$_W['template']}/{$name}/{$file_path}.tpl.php";
        } else {
            $source = SDABC_TPL . "{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/app/themes/{$_W['template']}/{$name}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/app/themes/default/{$name}/{$file_path}.html";
            is_file($source) or $source = IA_ROOT . "/app/themes/{$_W['template']}/{$file_path}.html";
            $compile = IA_ROOT . "/data/tpl/app/{$_W['template']}/{$name}/{$file_path}.tpl.php";
        }
        is_file($source) or exit("Error: template source '{$file_path}' is not exist!");

        $paths = pathinfo($compile);
        // Setting the prefix of cache file with $_W['uniacid']
        $compile = str_replace($paths['filename'], $_W['uniacid'] . '_' . $paths['filename'], $compile);
        // Update cache file
        if (DEVELOPMENT || !is_file($compile) || filemtime($source) > filemtime($compile)) {
            template_compile($source, $compile, true);
        }
        // Import variables into the current symbol table from $context
        extract($context);
        include $compile;
    }

    /**
     * Create web URL
     * @param $segment string   "$do/$cn/$cm"
     * @param array $params
     * @return string
     */
    public function wUrl($segment, $params = array())
    {
        list($do, $cn, $cm) = explode('/', $segment);
        $segment = "site/entry/{$do}";
        $params['cn'] = $cn;
        $params['cm'] = $cm;
        $params['m'] = $this->modulename;
        return wurl($segment, $params);
    }

    /**
     * Create app URL
     * @param string $segment "$do/$cn/$cm"
     * @param array $params
     * @param bool $no_direct
     * @param bool $add_host
     * @return string
     */
    public function mUrl($segment, $params = array(), $no_direct = true, $add_host = false)
    {
        list($do, $cn, $cm) = explode('/', $segment);
        $segment = "entry//{$do}";
        $params['cn'] = $cn;
        $params['cm'] = $cm;
        $params['m'] = $this->modulename;
        return murl($segment, $params, $no_direct, $add_host);
    }

    /**
     * Employee menus
     * @return array
     */
    public function _employeeMenus()
    {
        return array(
            array(
                'text' => '员工列表',
                'url' => $this->wUrl('employee/web_employee_list/index'),
                'active' => 'web_employee_list',
            ),
            array(
                'text' => '添加员工',
                'url' => $this->wUrl('employee/web_employee_add/index'),
                'active' => 'web_employee_add',
            ),
            array(
                'text' => '工号管理',
                'url' => $this->wUrl('employee/web_employee_id/index'),
                'active' => 'web_employee_id',
            ),
//            array(
//                'text' => '设置',
//                'url' => $this->wUrl('employee/web_employee_setting/index'),
//                'active' => 'web_employee_setting',
//            ),
        );
    }

    /**
     * Employee menus
     * @return array
     */
    public function _departmentMenus()
    {
        return array(
            array(
                'text' => '部门列表',
                'url' => $this->wUrl('department/web_department_list/index'),
                'active' => 'web_department_list',
            ),
            array(
                'text' => '添加部门',
                'url' => $this->wUrl('department/web_department_add/index'),
                'active' => 'web_department_add',
            ),
        );
    }
}