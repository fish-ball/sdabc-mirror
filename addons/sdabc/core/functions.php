<?php
/**
 * module functions
 */

defined('IN_IA') or exit('Access Denied!');


/**
 * Require and instance controller class
 * @param $cn string class name
 * @param array $params
 * @return object
 */
function ec_controller($cn, $params = array())
{
    $path = str_replace('_', '/', $cn);
    $file = SDABC_CTL . "{$path}.php";
    is_file($file) ?
        require_once $file :
        message('未找到控制器文件：' . $file, referer(), 'error');
    class_exists($cn) or message('未找到控制器类：' . $cn, referer(), 'error');
    $cls = new ReflectionClass($cn);
    return $cls->newInstanceArgs($params);
}

/**
 * Require and instance model class
 * @param string $cn    the name of model class,
 *          note: the file name must be the same as class name
 * @param array $param the param when instance class
 * @return object
 */
function ec_model($cn, $param = array())
{
    $file = SDABC_MODEL . "{$cn}.class.php";
    is_file($file) ?
        require_once $file :
        message('未找到模型类文件：' . $file, referer(), 'error');
    class_exists($cn) or message('模型类不存在：' . $cn, referer(), 'error');
    $cls = new ReflectionClass($cn);
    return $cls->newInstanceArgs($param);
}

/**
 * Get static file URL
 * @param $path
 * @return string
 */
function ec_static($path)
{
    $file = SDABC_STATIC . $path;
    is_file($file) or message('未找到静态文件：' . $file, referer(), 'error');
    return tomedia($file);
}

/**
 * Get the file in the path
 * @param $path string directory path
 * @return array the file in path
 */
function ec_get_files($path)
{
    $files = array();
    if (is_dir($path)) {
        if ($dh = opendir($path)) {
            while ($file = readdir($dh)) {
                if ($file != '.' && $file != '..') {
                    $file = $path . $file;
                    is_file($file) ?
                        $files[] = $file :
                        $files = array_merge($file, ec_get_files($file . '/'));
                }
            }
            closedir($dh);
        }
    }
    return $files;
}