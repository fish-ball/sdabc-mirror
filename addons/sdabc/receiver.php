<?php
/**
 * 顺德农行模块订阅器
 *
 * @author easecloud
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');

class SdabcModuleReceiver extends WeModuleReceiver {
	public function receive() {
	    global $_W;
        // 当用户关注订阅号时，处理事件
        // 粉丝关注订阅号之后，查看有没有被员工绑定
        // 如果有，则确认绑定关系，即这个粉丝就是该员工拓展进来的
        // 注意：【系统】->【订阅管理】中，一定要启用该模块
        if ($this->message['event'] == 'subscribe') {
            $fan_uid = pdo_getcolumn('mc_mapping_fans', array(
                'uniacid' => $_W['uniacid'],
                'openid' => $this->message['from'],
                ), 'uid') and
            // 粉丝在订阅号的信息
            $fan = pdo_get('mc_members', array(
                'uniacid' => $_W['uniacid'],
                'uid' => $fan_uid,
            )) and
            // 服务号的ID（员工所在的服务号）
            $employee_uniacid = pdo_getcolumn('ec_sdabc_setting', array(
                'subscription_uniacid' => $_W['uniacid'],
            ), 'uniacid') and
            // 根据粉丝当前的昵称查找出粉丝在服务号中的 uid
            $fan_uid2 = pdo_getcolumn('mc_members', array(
                'uniacid' => $employee_uniacid,
                'nickname' => $fan['nickname'],
            ), 'uid') and
            // 确认粉丝和员工的绑定关系，is_follow = 1
            pdo_update('ec_sdabc_employee_fans', array('is_follow' => 1), array(
                'uniacid' => $employee_uniacid,
                'subscription_uniacid' => $_W['uniacid'],
                'fans_uid' => $fan_uid2,
            ));
        }
	}
}